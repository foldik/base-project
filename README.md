# Alap maven projekt minta

Ez egy egyszerű kis maven-es projekt minta néhány segítő script-el.

### Használat:

- Git bash-ből add ki az alábbi parancsokat (figyelj oda, hogy van ahol érdemes átírnod a parancsot értelemszerűen):

```
git clone https://gitlab.com/foldik/base-project.git my_new_project_name
```

```
cd my_new_project_name
```

- Válassz ki egy template-et az alábbi parancsok valamelyikével:
 - ``` git checkout spring.basic ```: egyszerű Spring-es projekt
 - ``` git checkout spring.boot.basic ```: egyszerű Spring Boot-os projekt
 - ``` git checkout spring.boot.web ```: egyszerű Spring Boot-os web-es projekt, azonnal indítható, böngészőből rögtön eléred a [http://localhost:8997/myboot/hello](http://localhost:8997/myboot/hello)-on :slight_smile:


- Csinálj egy új repót gitlab-on. Url: [https://gitlab.com/](https://gitlab.com/)
- Másold be az új projekted url-jét a ```reset``` fájlba

``` reset ```

```
#!/bin/bash

REPO_URL="IDE_MÁSOLD_BE_AZ_URLT"

echo '# My New Project' > README.md

rm -rf .git && git init && git remote add origin $REPO_URL
```

__Végül futtasd le az alábbi parancsot Git bash-ből:__

Soha ne futtasd le újból ebben a projektben!!!

```
./reset
```

__Mostantól bármikor amikor fel szeretnéd küldeni a módosításaidat a master-re csak add ki az alábbi parancsot a base-project mappából:__

```
./up
```

__Megnyitás Spring Tool Suite-ből:__

File :arrow_right: Open Projects from File Systems... :arrow_right: Directory...

Válaszd ki a __my_new_project_name__ mappát, majd :slight_smile: __Finish__.
