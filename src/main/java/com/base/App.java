package com.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.core.env.Environment;

@SpringBootApplication
@ServletComponentScan
public class App implements CommandLineRunner {

    @Autowired
    private Environment environment;

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        /*
            If you want to execute some code then put it here and not into the main method.
            This is because of the Spring Boot :). Of course you can delete the code below.
         */
        System.out.println("My App Started. Hit me on the " + getHelloServletUrl());
    }

    private String getHelloServletUrl() {
        return "http://localhost:" + environment.getRequiredProperty("server.port", Integer.class) + environment.getRequiredProperty("server.contextPath") + "/hello";
    }
}
