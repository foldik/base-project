package com.base.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
// @ComponentScan("com.base")
// @PropertySource("classpath:application.properties")
// These annotations are not needed because the @SpringBootApplication on the App.class handles all of it.
// Only left here to not forget their existence
public class Config {

    @Autowired
    private Environment environment;
}
